import { useState } from 'react'
import './App.css'
import Text from './Components/Data'



function App() {
  
  const [count, setCount] = useState(0);
  const [text, setText] = useState([]);
//-------------------------------------------
  const handleSubmit = (e) =>{
    e.preventDefault();

    let amount = parseInt(count);
    if(count <= 0){
      amount = 0;
    }
    if(count > 229){
      amount = 229;
    }
    setText(Text.slice(0,amount))
  }
//-------------------------------------------
  return (
    <>
      <section className='section-center'>
        <h3>Genrate Lorem ipsum</h3>
        <form onSubmit={handleSubmit}>
          <input 
            type="number"
            name='amount'
            id='amount'
            value={count}
            onChange={ (e) => setCount(e.target.value)}
            />
            <button className='btn'> Genrate </button>
            <article>
              {
                text.map( (item, index) => {
                  return <p key={index}>{item}</p>
                })
              }
            </article>
        </form>

      </section>

    </>
  )
}

export default App
